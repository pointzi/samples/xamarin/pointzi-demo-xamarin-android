﻿
using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidLSamples.Utils;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace AndroidLSamples
{
	[Activity (Label = "Palette Example", ParentActivity=typeof(HomeActivity))]			
	public class ImageDetailPaletteActivity : Activity
	{
		PhotoItem item;
		TextView name;
		LinearLayout colors;
		protected async override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.activity_image_detail);
			ActionBar.SetDisplayHomeAsUpEnabled (true);
			ActionBar.SetDisplayShowHomeEnabled (true);
			ActionBar.SetIcon (Android.Resource.Color.Transparent);

			var id = Intent.GetIntExtra ("id", 0);
			item = Photos.GetPhoto (id);
			if (item == null)
				return;

			name = FindViewById<TextView> (Resource.Id.name);
			colors = FindViewById<LinearLayout> (Resource.Id.colors);
			var image = FindViewById<ImageView> (Resource.Id.image);

			image.SetImageResource (item.Image);
			name.Text = ActionBar.Title = item.Name;

			FindViewById<Button> (Resource.Id.apply_palette).Click += async (sender, e) => {

				var bitmap = await BitmapFactory.DecodeResourceAsync (Resources, item.Image);

				//generates the pallet with 16 samples(default)
				//Contact images/avatars: optimal values are 24-32
				//Landscapes: optimal values are 8-16
			};

		}

       
	}
}

