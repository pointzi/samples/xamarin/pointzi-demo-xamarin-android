﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidLSamples
{

	public class RecyclerItem : Java.Lang.Object
	{
		static int[] Ids = new int[]{Resource.Drawable.caterpiller, Resource.Drawable.evolve, Resource.Drawable.flying_in_the_light_large, Resource.Drawable.jelly_fish_2, Resource.Drawable.lone_pine_sunset, Resource.Drawable.look_me_in_the_eye, Resource.Drawable.over_there, Resource.Drawable.rainbow, Resource.Drawable.rainbow, Resource.Drawable.sample1, Resource.Drawable.sample2};
		static int nextId;
		public RecyclerItem()
		{
			Image = Ids[nextId];
			nextId++;
			if (nextId >= Ids.Length)
				nextId = 0;
		}
		public string Title {get;set;}
		public int Image {get;set;}
	}

	[Activity (Label = "RecyclerView Example", ParentActivity=typeof(HomeActivity))]			
	public class RecyclerViewActivity : Activity
	{
		          
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.activity_recycler_view);
			ActionBar.SetDisplayHomeAsUpEnabled (true);
			ActionBar.SetDisplayShowHomeEnabled (true);
			ActionBar.SetIcon (Android.Resource.Color.Transparent);
   
		}
			
	}

   

   


}

