#!/bin/sh -x
set -e
nuget restore
msbuild /p:Configuration=Release /t:Clean\;Rebuild
/usr/local/bin/puck -submit=auto -download=true -notes="$(git log -1)" -notes_type=markdown -source_path=$PWD -repository_url=git@bitbucket.org:shawk/pointzi-demo-xamarin.git -api_token=$HOCKEYAPP_TOKEN -app_id=$HOCKEYAPP_APPID build/outputs/SHDynamic.ipa
